import logging
from data import db
from datetime import datetime, timezone

logger = logging.getLogger(__name__)


def get_posts(q=None, limit=100):
    """Get posts
    """
    return [db[post_id] for post_id in list(db.keys())[:limit]]


def get_post(post_id):
    """Get an individual post
    """
    if post_id not in db:
        logger.warning(f"Post {post_id} not found")
        return 'Not Found', 404
    return db[post_id]


def put_post(post_id, post=None):
    """Update a post
    """
    current = db.get(post_id)
    # created and/or updated timestamp, set to utc
    now = datetime.now(timezone.utc)

    if current:
        # update
        current.update(post)
        current['updated_at'] = now

        logger.info(f"Post {post_id} updated")
        return 'Updated', 200
    else:
        # create
        post['id'] = post_id
        post['created_at'] = post['updated_at'] = now
        db[post_id] = post

        logger.info(f"Post {post_id} created")
        return 'Created', 201


def delete_post(post_id):
    """Delete a post
    """
    try:
        del db[post_id]
        logger.info(f"Post {post_id} deleted")
    except KeyError:
        logger.warning(f"Post {post_id} not found")
        return 'Not Found', 404
