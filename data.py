from collections import OrderedDict

# A dict with keys ordered by insertion as a simple in-memory database.
#
# Replace with your own database and access type (mongodb,
# sqlite, db-api, sqlalchemy ...)
db = OrderedDict()
