from typing import Union

def say_hello(name: str) -> str:
    return f"Hoi {name}!"


def wrong_return(value: Union[int, float]) -> str:
    return 10


class Test:
    db: dict

t = Test()
t.db = "funny thing"


say_hello(10)
wrong_return(10)
wrong_return(10.75)
wrong_return('hi!')
