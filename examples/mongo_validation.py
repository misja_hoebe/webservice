import pymongo
from datetime import datetime

schema = {
    "bsonType": "object",
    "properties": {
        "_id": {
            "bsonType": "objectId"
        },
        "name": {
            "type": "string",
            "description": "A person's name"
        },
        "dob": {
            "bsonType": "date"
        },
        "gender": {
            "type": "string",
            "enum": ["M", "F", "U"]
            # "default": "U" # not supported
        }
    },
    "required": ["name"],
    "additionalProperties": False
}

conn = pymongo.MongoClient("mongodb://10.172.60.189")
db = conn['test']

if 'people' not in db.collection_names():
    db.create_collection(
        'people', validator={"$jsonSchema": schema})
    db.people.create_index('name', unique=True)

data = {
    "name": "Jane",
    "dob": datetime(2018, 3, 18),
    "gender": "F"
}

db.people.insert_one(data)
print('insert:', data)

result = db.people.find_one({'name': 'Jane'})
print('find:', result)

# db.drop_collection('people')
