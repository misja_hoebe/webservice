import uuid
import json
from bson import json_util
from datetime import datetime, timezone

data = {
    'id': uuid.uuid4(),
    'first_name': 'Misja',
    'last_name': 'Hoebe',
    'created_at': datetime.now(timezone.utc)
}

print(data)

#json.dumps(data)


class MyEncoder(json.JSONEncoder):
    """My special encoder
    """
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        if isinstance(obj, uuid.UUID):
            return str(obj)
        return super().default(obj)


result = json.dumps(data, cls=MyEncoder)
print(result)


# MongoDB

mongo = json_util.dumps(data)
print(mongo)

native = json_util.loads(mongo)
print(native)
