from jsonschema import validate, FormatChecker

schema = {
    "type": "object",
    "properties": {
        "name": {
            "type": "string",
            "description": "A person's name"
        },
        "dob": {
            "type": "string",
            "format": "date"
        },
        "gender": {
            "type": "string",
            "enum": ["M", "F", "U"],
            "default": "U"
        }
    },
    "required": ["name"],
    "additionalProperties": False
}

data = {
    "name": "Jane",
    "dob": "1980-03-18",
    #"gender": "U"
}

validate(data, schema)
# validate(data, schema, format_checker=FormatChecker())

print(data)
# what about default?
