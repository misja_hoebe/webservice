# A Blog API Service

A simple API service implemented with [Connexion](https://github.com/zalando/connexion) wrapping a [Flask](http://flask.pocoo.org/) app.

As this is a sample application, some things will need to get changed:

1. It features a simple in-memory store implemented as a ``dict``. This isn't very safe, implement a proper data store using one of the techniques used during the course (``mongodb`` or ``sqlite``, the latter implemented via either ``db-api`` or ``sqlalchemy``). Please note: the ``Blog`` post model defined in the openapi specification implements ``id`` as its primary key and change this according to your need, e.g. when using ``mongodb`` an ``_id`` field would be more sensible as it is created automatically on insert.

2. Adding and editing posts is folded into a single endpoint ``/posts/{post_id}`` as a ``PUT`` request. This isn't a very restful pattern, a ``PUT`` request method should ideally be used for modifying a resource only. Refactor the openapi service specification and implementation to have a ``POST`` request method at the ``/posts`` endpoint for creating posts. The existing ``PUT`` request method at ``/posts/{post_id}`` should consequently only be responsible for updating existing posts.

3. You might want to modify some other things along the way: the landing page contains a counter indicating how many posts are hosted by this service. This count is based on the current datastore so should change to reflect your datastore implementation.

4. To be nice and informative, add an about page at ``/about`` to inform users who is responsible for this service and ways to get in touch.

5. (Optional) The ``/posts`` (``GET``) endpoint accepts two additionals parameters, ``q`` for searching posts containing a search term  and ``limit`` for limiting the number of posts to return. Handle these two parameters when querying your datastore.

## Requirements

```bash
pip install connexion
pip install connexion[swagger-ui]
```
