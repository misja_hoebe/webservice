import flask
import logging
import connexion
from data import db

service = connexion.FlaskApp(__name__, specification_dir='config/')
service.add_api('blog_service.yaml')


# Connexion wraps a Flask app, to which we can add additional routes ...
@service.app.route('/', methods=['GET'])
def index():
    return flask.render_template('index.html', count=len(db))


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    # NOTE: host and port should match as defined in the openapi definition
    service.run(host='127.0.0.1', port=8081, debug=True)
